from django.apps import AppConfig


class VikmessageappConfig(AppConfig):
    name = 'VikMessageApp'

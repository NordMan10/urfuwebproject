# Generated by Django 3.1.1 on 2020-11-15 20:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('VikMessageApp', '0003_message'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='message',
            options={'verbose_name': 'Сообщение', 'verbose_name_plural': 'Сообщения'},
        ),
        migrations.AlterModelOptions(
            name='user',
            options={'verbose_name': 'Пользователь', 'verbose_name_plural': 'Пользователи'},
        ),
        migrations.AddField(
            model_name='user',
            name='phone_number',
            field=models.CharField(default='', max_length=12, verbose_name='Номер телефона'),
        ),
    ]

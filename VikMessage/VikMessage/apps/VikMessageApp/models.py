from django.db import models
from django.utils import timezone


class User(models.Model):
    first_name = models.CharField("Имя", max_length=64)
    last_name = models.CharField("Фамилия", max_length=64)
    phone_number = models.CharField("Номер телефона", max_length=12, default="", unique=True)

    def __str__(self):
        return self.first_name + " " + self.last_name

    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"


class Message(models.Model):
    from_id = models.ForeignKey(User, on_delete=models.CASCADE, related_name='sender')
    to_id = models.ForeignKey(User, on_delete=models.CASCADE, related_name='receiver')
    text = models.TextField("Текст сообщения")
    send_dateTime = models.DateTimeField(default=timezone.now, verbose_name='Дата и время отправки')
    edit_dateTime = models.DateTimeField(default=timezone.now, verbose_name='Дата и время редактирования')

    def __str__(self):
        return self.text

    class Meta:
        verbose_name = "Сообщение"
        verbose_name_plural = "Сообщения"

class Test(models.Model):
    field1 = models.IntegerField()
    field2 = models.CharField("field2", max_length=50)

    def __str__(self):
        return "field2 = " + self.field2

# class Chat(models.Model):
#     chat_Id = models.IntegerField(primary_key=True)
#     creator_id = models.ForeignKey(User, on_delete=models.CASCADE)
#     name = models.CharField('Название чата', max_length=100)
#
#     def __str__(self):
#         return self.name + " id: " + self.id
#
#
# class Conversation(models.Model):
#     conversation_Id = models.IntegerField(primary_key=True)
#     creator_id = models.ForeignKey(User, on_delete=models.CASCADE)
#     name = models.CharField('Название чата', max_length=100)
#
#     def __str__(self):
#         return self.name + " id: " + self.id
#
#
# class CommunicationAttendees(models.Model):
#     user_id = models.ForeignKey(User, on_delete=models.CASCADE)
#     chat_id = models.ForeignKey(Chat, on_delete=models.CASCADE)
#     conversation_id = models.ForeignKey(Conversation, on_delete=models.CASCADE)
#
#     def __str__(self):
#         return "It's a reference between attendees and communication"

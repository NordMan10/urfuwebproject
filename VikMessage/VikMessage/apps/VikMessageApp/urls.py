from django.urls import path

from . import views

app_name = "VikMessage"

urlpatterns = [
    path("", views.main, name="main"),
    path("signUp/", views.signUp, name="signUp"),
    path("users/", views.index, name="index"),
    path("<int:user_id>/", views.detail, name="detail")
]


from django.shortcuts import render
from .models import User, Message
from django.http import Http404, HttpResponseRedirect


def main(request):
    return render(request, "VikMessage/main.html")


def signUp(request):
    try:
        user = User.objects.get(phone_number=request.POST['phone'])
    except:
        user = User()
    return render(request, "VikMessage/signUp.html")


def index(request):
    latest_users = User.objects.order_by("-first_name")[:5]
    return render(request, "VikMessage/list.html", {"latest_users": latest_users})


def detail(request, user_id):
    try:
        user = User.objects.get(id=user_id)
    except:
        raise Http404("User have not found!")
    return render(request, "VikMessage/detail.html", {"user": user})
